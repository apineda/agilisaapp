﻿using System;
using SQLite.Net;
using SQLite.Net.Interop;
using System.IO;
using SQLite.Net.Async;

namespace AgilisaApp.Core
{

    public interface IDatabaseHelper
    {
        void InitializeDatabase();
        SQLiteConnection GetConnection();
    }

    public class DatabaseHelper : IDatabaseHelper
    {
        SQLiteConnection con;
        SQLiteAsyncConnection conAsync;
        ISQLitePlatform _platform;

        string _path;
        readonly string _databaseName = "Agilisa.db3";

        public string DbPath{ get{return  _path;}}


        public DatabaseHelper(ISQLitePlatform platform, IAppDefaults defaults)
        {
            var finalPath = Path.Combine(defaults.DatabasePath, _databaseName);

            _platform = platform;
            _path = finalPath;

            con = new SQLiteConnection(_platform, DbPath);

        }

        public void InitializeDatabase()
        {
            con.CreateTable<Usuario>(CreateFlags.None);
            con.CreateTable<Producto>(CreateFlags.None);
            con.CreateTable<Languagues>(CreateFlags.None);
        }

        public SQLiteConnection GetConnection()
        {
            if (con == null)
            {
                con = new SQLiteConnection(_platform, DbPath);
            }

            return con;
        }

        public SQLiteAsyncConnection GetConnectionAsync()
        {
            if (conAsync == null)
            {
//                conAsync = new SQLiteAsyncConnection(() => GetConnection());
            }

            return conAsync;
        }

    }
}

