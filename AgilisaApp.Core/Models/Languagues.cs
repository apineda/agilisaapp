﻿using System;
using SQLite.Net.Attributes;

namespace AgilisaApp.Core
{
    public class Languagues
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [Column(name:"ServerId")]
        public int OnlineId { get; set; }

        [Ignore]
        public string NamePlusDescription { get; set; }

        public Languagues()
        {
        }
    }
}

