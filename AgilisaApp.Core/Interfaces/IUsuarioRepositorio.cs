﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AgilisaApp.Core
{
    public interface IUsuarioRepositorio
    {
        Usuario Add(Usuario usuario);
        Usuario GetById(int Id);
        List<Usuario> GetAll();
    }

    public class UsuarioRepositorio : IUsuarioRepositorio
    {
        readonly IDatabaseHelper _db;


        public UsuarioRepositorio(IDatabaseHelper db)
        {
            _db = db;
        }

        #region IUsuarioRepositorio implementation
        public Usuario Add(Usuario usuario)
        {
            var inserted = _db.GetConnection().Insert(usuario);

            if (inserted > 0)
            {
                return _db.GetConnection().Table<Usuario>().LastOrDefault();
            }

            return null;
        }

        public Usuario GetById(int Id)
        {
            var usuario = _db.GetConnection().Find<Usuario>(Id);
                
            return usuario;
        }

        public List<Usuario> GetAll()
        {
            return _db.GetConnection().Table<Usuario>().ToList();
        }

        #endregion
        
    }
}

