﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AgilisaApp.Core
{

    public interface IProductoRepository
    {
        Producto Add(Producto producto);

        List<Producto> GetAll();

        Producto GetById(long id);
    }

    public class ProductoRepositorio : IProductoRepository
    {
        IDatabaseHelper _db;

        public ProductoRepositorio(IDatabaseHelper db)
        {
            _db = db;
        }

        #region IProductoRepository implementation

        public Producto Add(Producto producto)
        {
            var id = _db.GetConnection().Insert(producto);

            return  (id > 0) ? _db.GetConnection().Table<Producto>().LastOrDefault() : null;
        }


        public List<Producto> GetAll()
        {
            return _db.GetConnection().Table<Producto>().ToList();
        }

        public Producto GetById(long id)
        {
            return _db.GetConnection().Find<Producto>(id);
        }
        #endregion
    }
}

