﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;

namespace AgilisaApp.Core
{
    public interface IAGOnlinerepositorio
    {
        Task<Usuario> AutenticaUsuarioFake(string usuario, string password);
        Task<Usuario> AutenticaUsuario(string usuario, string password);
    }

    public class AGOnlinerepositorio :IAGOnlinerepositorio
    {
        #region IAGOnlinerepositorio implementation
        public Task<Usuario> AutenticaUsuarioFake(string usuario, string password)
        {
            var usr = new Usuario
                {Nombre = "Jose", Apellido = "Perez", Id=1, Username="jperez@gmail.com" };

            return Task.FromResult<Usuario>(usr);
        }

        public async Task<Usuario> AutenticaUsuario(string usuario, string password)
        {
            IAGOnlineClient client = AGOnlineClientFactory.Create();

            var resource = string.Format("Account/SignInUser?email={0}&password={1}", usuario, password);

            var response = await client.GetAsync(resource);

            return await response.ParseSingleRecord<Usuario>();
        }

        #endregion
        
    }


    public static class HttpResponseExtension
    {
        public static async Task<T> ParseSingleRecord<T> (this HttpResponseMessage response) 
        {
            if (response != null)
            {
                var content = response.Content;

                if (content != null)
                {
                    var read = await content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(read);
                }
            }

            return default(T);
        }
    }
}

