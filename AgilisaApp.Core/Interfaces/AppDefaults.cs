﻿using System;

namespace AgilisaApp.Core
{
    public class AppDefaults : IAppDefaults
    {
        #region IAppDefaults implementation

        public string DatabasePath { get; set;}

        #endregion

        public AppDefaults()
        {
        }
    }
}

