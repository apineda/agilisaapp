﻿using System;

namespace AgilisaApp.Droid
{
    public class ContextParameter
    {
        public readonly static string ID = "id";
        public readonly static string USER_NAME = "username";
        public readonly static string NAME = "name";
        public readonly static string LAST_NAME = "last_name";

        public ContextParameter()
        {
        }
    }
}

