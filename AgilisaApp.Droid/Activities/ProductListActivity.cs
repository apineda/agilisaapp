﻿

using Android.App;
using Android.OS;
using Android.Widget;
using TinyIoC;
using AgilisaApp.Core;
using Android.Content;

namespace AgilisaApp.Droid
{
    [Activity(Label = "ProductListActivity")]            
    public class ProductListActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.ProductList);

            var listView = FindViewById<ListView>(Resource.Id.productListView);

            var repositorio = TinyIoCContainer.Current.Resolve<IProductoRepository>();

            var productAdapter = new ProductsAdapter(this, repositorio);

            listView.Adapter = productAdapter;

            listView.ItemClick += OnListViewItemClicked;

        }

        void OnListViewItemClicked (object sender, AdapterView.ItemClickEventArgs e)
        {
            var intent = new Intent(this, typeof(ProductDetailsActivity));

            intent.PutExtra("PRODUCTO_ID", e.Id);

            StartActivity(intent);
        }
    }
}

