﻿

using Android.App;
using Android.OS;
using System.Threading.Tasks;
using AgilisaApp.Core;
using TinyIoC;
using SQLite.Net.Interop;

namespace AgilisaApp.Droid
{
    [Activity(Label = "Agilisa App", MainLauncher = true, Icon = "@drawable/icon",  NoHistory=true) ]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);


            App.Initilize();

            var appDefaults = TinyIoCContainer.Current.Resolve<IAppDefaults>();

            appDefaults.DatabasePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            SetContentView(Resource.Layout.Splash);

            InitializeDatabase();

        }

        protected override async void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);

            await Task.Delay(2000);

            StartActivity(typeof(LoginActivity));
        }


        private void InitializeDatabase()
        {
            IDatabaseHelper dbHelper = TinyIoCContainer.Current.Resolve<IDatabaseHelper>();

            dbHelper.InitializeDatabase();
        }
    }
}

