﻿
using Android.App;
using Android.OS;
using Android.Widget;
using AgilisaApp.Core;
using TinyIoC;

namespace AgilisaApp.Droid
{
    [Activity(Label = "ProductDetails", ParentActivity=typeof(ProductListActivity))]            
    public class ProductDetailsActivity : Activity
    {
        IProductoRepository _productoRepository;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            _productoRepository = TinyIoCContainer.Current.Resolve<IProductoRepository>();

            SetContentView(Resource.Layout.ProductDetails);

            var id = FindViewById<TextView>(Resource.Id.productIdTextView);
            var name = FindViewById<TextView>(Resource.Id.productNombreTextView);
            var price = FindViewById<TextView>(Resource.Id.productPrecioTextView);
            var quantity = FindViewById<TextView>(Resource.Id.productCantidadTextView);

            // Create your application here

            var Id = Intent.GetLongExtra("PRODUCTO_ID", -1);

            if (Id > -1)
            {
                var producto = _productoRepository.GetById(Id);

                if (producto != null)
                {
                    id.Text = producto.Id.ToString();
                    name.Text = producto.Nombre;
                    price.Text = producto.Precio.ToString();
                    quantity.Text = producto.Cantidad.ToString();
                }
            }

        }
    }
}

