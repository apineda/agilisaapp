﻿

using Android.App;
using Android.OS;
using Android.Widget;
using SQLite.Net.Platform.XamarinAndroid;
using AgilisaApp.Core;
using System.Collections.Generic;

namespace AgilisaApp.Droid
{
    [Activity(Label = "HomeActivity")]            
    public class HomeActivity : Activity
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public int Id { get; set; }

        private IUsuarioRepositorio _usuarioRepositorio;
        private IProductoRepository _productoRepositorio;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Home);

            var userInfoTextView = FindViewById<TextView>(Resource.Id.userInfoTextView);
            var showProductsButton = FindViewById<Button>(Resource.Id.showProducts);

            _productoRepositorio = TinyIoC.TinyIoCContainer.Current.Resolve<IProductoRepository>();

            showProductsButton.Click += (sender, e) => StartActivity(typeof(ProductListActivity));

            AsignData();

            userInfoTextView.Text = string.Format("Welcome back {0} {1}!!, \n Username: {2}", Name, LastName, UserName);

            //FillProducts();


        }

        void AsignData()
        {
            _usuarioRepositorio = TinyIoC.TinyIoCContainer.Current.Resolve<IUsuarioRepositorio>();

            Id = Intent.GetIntExtra(ContextParameter.ID, -1);

            var usrLocal = _usuarioRepositorio.GetById(Id);

            if (usrLocal != null)
            {
                Name = usrLocal.Nombre;
                LastName = usrLocal.Apellido;
                UserName = usrLocal.Username;

            }

        }

        void FillProducts()
        {
            var products = new List<Producto>()
            {
                new Producto
                {
                    Nombre = "Jabon de Pasta",
                    Id = 1,
                    Precio = 23.18M,
                    Cantidad = 100
                },

                new Producto
                {
                    Nombre = "Pasta de diente colgate",
                    Id = 2,
                    Precio = 45.18M,
                    Cantidad = 29
                },

                new Producto
                {
                    Nombre = "Televisor 32 pulgadas LCD",
                    Id = 3,
                    Precio = 13900,
                    Cantidad = 5
                },
                    new Producto
                    {
                        Nombre = "Radio AM/FM Sony",
                        Id = 4,
                        Precio = 1200.99M,
                        Cantidad = 145
                    }
            };


            foreach (var item in products)
            {
                _productoRepositorio.Add(item);
            }
        }
    }
}

