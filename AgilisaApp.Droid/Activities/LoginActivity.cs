﻿

using Android.App;
using Android.OS;
using Android.Widget;
using AgilisaApp.Core;
using Android.Content;
using SQLite.Net.Platform.XamarinAndroid;
using TinyIoC;

namespace AgilisaApp.Droid
{
    [Activity(Label = "Login", Icon = "@drawable/icon")]
    public class LoginActivity : Activity
    {
        private IAGOnlinerepositorio _onlineRepositorio;
        private IUsuarioRepositorio _usuarioRepositorio;

        EditText _userEditText;
        EditText _passwordEditText;
        Button _loginButton;


        public LoginActivity()
        {
            _onlineRepositorio = TinyIoCContainer.Current.Resolve<AGOnlinerepositorio>();
            _usuarioRepositorio = TinyIoCContainer.Current.Resolve<IUsuarioRepositorio>();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Login);

            _userEditText = FindViewById<EditText>(Resource.Id.userEditText);
            _passwordEditText = FindViewById<EditText>(Resource.Id.pswEditText);
            _loginButton = FindViewById<Button>(Resource.Id.loginBtn);

            _loginButton.Click += OnLoginClick;
                
        }

        public async void OnLoginClick(object sender, System.EventArgs e)
        {
            var user = await _onlineRepositorio.AutenticaUsuarioFake(_userEditText.Text, _passwordEditText.Text);


            if (user != null)
            {

                var userLocal = _usuarioRepositorio.Add(new Usuario
                    {
                        Nombre = user.Nombre,
                        Apellido = user.Apellido + " - Local",
                        Email = user.Email,
                        Username = user.Username
                    });

                var intent = new Intent(this, typeof(HomeActivity));
//                intent.PutExtra(ContextParameter.USER_NAME, user.Username);
                intent.PutExtra(ContextParameter.ID, userLocal.Id);
//                intent.PutExtra(ContextParameter.NAME, user.Nombre);
//                intent.PutExtra(ContextParameter.LAST_NAME, user.Apellido);
                StartActivity(intent);
            }
        }
    }
}

