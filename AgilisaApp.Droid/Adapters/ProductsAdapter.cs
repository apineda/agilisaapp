﻿using Android.Views;
using Android.Widget;
using AgilisaApp.Core;
using System.Collections.Generic;
using Android.App;
using System.Linq;

namespace AgilisaApp.Droid
{
    public class ProductsAdapter : BaseAdapter
    {

        List<Producto> _productList;
        IProductoRepository _productRepository;
        Activity _activity;

        public ProductsAdapter(Activity activity, IProductoRepository productRepository)
        {
            _activity = activity;
            _productRepository = productRepository;

            FillProducts();
        }

        void FillProducts()
        {
            _productList = _productRepository.GetAll();
        }

        #region implemented abstract members of BaseAdapter
        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }
        public override long GetItemId(int position)
        {
            return _productList[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? _activity.LayoutInflater.Inflate (Resource.Layout.ProductListItem, parent, false);
            
            var productName = view.FindViewById<TextView> (Resource.Id.productNameTextView);
            var productQuantity = view.FindViewById<TextView> (Resource.Id.productQuantityTextView);
            var productPrice = view.FindViewById<TextView> (Resource.Id.productPriceTextView);

            productName.Text = _productList[position].Nombre;
            productQuantity.Text = string.Format("Quantity: {0}", _productList[position].Cantidad);
            productPrice.Text = string.Format("Price {0}", _productList[position].Precio);


            return view;
        }

        public override int Count
        {
            get { return _productList.Count();}
        }
        #endregion

    }
}

