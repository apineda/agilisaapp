﻿using System;
using TinyIoC;
using AgilisaApp.Core;
using SQLite.Net.Interop;
using SQLite.Net.Platform.XamarinAndroid;

namespace AgilisaApp.Droid
{
    public static class App
    {
        public static void Initilize()
        {
            var container = TinyIoCContainer.Current;


            container.Register<ISQLitePlatform, SQLitePlatformAndroid>();
            container.Register<IAppDefaults, AppDefaults>();
            container.Register<IDatabaseHelper, DatabaseHelper>();

            container.Register<IUsuarioRepositorio, UsuarioRepositorio>();
            container.Register<IAGOnlinerepositorio, AGOnlinerepositorio>();
            container.Register<IProductoRepository, ProductoRepositorio>();

        }
    }
}

